# ContactsPrj

Contacts is a responsive web application for simple contact management that includes add, update, delete, search and list functionalities served by a backend Restful API.

## Get started

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.3.

### Clone the repo

```bash
git clone https://menatalla_mostafa@bitbucket.org/menatalla_mostafa/contactsapp.git
```

### Installation

Install the npm packages described in the package.json and verify that it works:
```bash
npm install
npm start
json-server --watch db.json
```




