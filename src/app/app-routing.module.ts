import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//npm install --save @types/es6-promise
const routes: Routes = [
  {
    path: '',
    redirectTo: '/contacts/list',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [{
      path: "contacts",
      loadChildren: () => import('./modules/contact/contact.module').then(m => m.ContactModule)
    }]
  },
  {
    path: '**',
    redirectTo: '/contacts/list'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
