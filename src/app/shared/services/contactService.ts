import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class contactService {
    public headers = new HttpHeaders();

    constructor(private http: HttpClient) { }
    url = 'http://localhost:4000/';

    list(): Observable<any> {
        return this.http.get(
            `${this.url}contacts`
            , { headers: this.headers });
    }

    listById(id): Observable<any> {
        return this.http.get(
            `${this.url}contacts/${id}`, { headers: this.headers }
        );
    }

    add(contact): Observable<any> {
        return this.http.post(
            `${this.url}contacts`, contact, { headers: this.headers }
        );
    }

    update(contact): Observable<any> {
        return this.http.put(
            `${this.url}contacts/${contact.id}`, contact, { headers: this.headers }
        );
    }

    delete(contact): Observable<any> {
        return this.http.delete(
            `${this.url}contacts/${contact.id}`, { headers: this.headers }
        );
    }
}
