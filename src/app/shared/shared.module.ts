
import { NgModule } from '@angular/core';
import { MaterialModule } from '../modules/material/material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        //modules
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        CommonModule
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        CommonModule
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    providers: [
    ]
})

export class SharedModule { }